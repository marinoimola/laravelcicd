<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_perkurangan()
    {
        $angka1 = 2;
        $angka2 = 3;
        $hasilPertambahan = $angka1 + $angka2;
        $hasilYangDiyakini = 5;
        $this->assertTrue($hasilPertambahan == $hasilYangDiyakini);
    }
}
